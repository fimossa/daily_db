import requests
from bs4 import BeautifulSoup
import pandas as pd
import threading, time
from multiprocessing import Pool
from fake_useragent import UserAgent
from time import sleep
from multiprocessing import Pool
from concurrent.futures import ThreadPoolExecutor
from bs4 import BeautifulSoup
from selenium import webdriver
import concurrent.futures
import urllib.request
import requests
import time
import sys
from datetime import datetime
import warnings

warnings.filterwarnings(action='ignore')


def last_page2(i):
    url = 'https://finance.naver.com/sise/sise_market_sum.nhn?sosok=' + str(i)
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    kk = soup.select('td a')
    last = 0
    for obj in kk:
        if '맨뒤' in obj.text:
            last = obj['href'].split('page=')[1]
    return int(last)


def name_code(i):
    tmp_list = []
    for page in range(1, last_page2(i) + 1):
        url = 'https://finance.naver.com/sise/sise_market_sum.nhn?sosok=' + str(i) + '&page=' + str(page)
        r = requests.get(url)
        html = BeautifulSoup(r.text, 'html.parser')
        articles = html.select("tbody td a")
        num = 1
        for obj in articles:
            # print(obj.text)
            if 'main' in obj['href']:
                # print(obj['href'])
                tmp_list.append([obj.text, obj['href'].split('=')[1]])
    return tmp_list


def search_stock_name(t_name):
    for obj in s_list:
        if obj[0] == t_name:
            return obj


def find_stock(url):
    t_code = url[1]
    t_name = url[0]
    df = pd.DataFrame()
    # t_code = df_kosdaq['code'][j]
    url = 'https://finance.naver.com/item/sise_day.nhn?code=' + t_code
    for page in range(1, last_page(t_code) + 1):
        pg_url = '{url}&page={page}'.format(url=url, page=page)
        df = df.append(pd.read_html(pg_url, header=0, encoding='euc-kr')[0], ignore_index=True)
    df = df.dropna()
    df = df.reset_index()
    return df


def average_price_ext(t_df, t_date):
    r_data = pd.DataFrame(index=list(range(0, len(t_df))), columns=list('a'))
    for i in range(t_date - 1, len(t_df)):
        r_data['a'][i] = t_df[i - t_date + 1:i].mean()
    return r_data['a']


def nan_change(t_str):
    if t_str == 'nan':
        return 'np.nan'
    else:
        return t_str

def data_write(ff, t_list):
    ff.write('\'' + t_list[0] + '\':(')
    for i in range(1, len(t_list) - 1):
        ff.write(nan_change(str(t_list[i])) + ', ')
    ff.write(nan_change(str(t_list[len(t_list) - 1])) + ')')


def last_page(t_code):
    url = 'https://finance.naver.com/item/sise_day.nhn?code=' + t_code
    r = requests.get(url)
    soup = BeautifulSoup(r.text, 'html.parser')
    kk = soup.select('td a')
    for obj in kk:
        if '맨뒤' in obj.text:
            last = obj['href'].split('page=')[1]
    return int(last)


if __name__ == "__main__":
    start_time = time.time()
    s_list = []
    s_list = s_list + name_code(0)
    s_list = s_list + name_code(1)
    print("---%s second ---" % (time.time() - start_time))
    stock_list = search_stock_name('삼성전자')
    print(stock_list)
    if stock_list == None:
        for i in range(100):
            stock_list = search_stock_name('삼성전자')
            if stock_list != None:
                break

    k = find_stock(stock_list)
    k = k.loc[::-1].reset_index()

    k_df = pd.DataFrame()
    k_df['Index'] = k['날짜']
    k_df['High'] = k['고가']
    k_df['Low'] = k['저가']
    k_df['Open'] = k['시가']
    k_df['Close'] = k['종가']
    k_df['Volume'] = k['거래량']
    k_df['Adj Close'] = k['종가']
    k_df['real'] = k['종가']
    # float('nan')

    for i in range(len(k_df)):
        k_df['Index'][i] = k_df['Index'][i].replace('.', '-', 10)

    for i in [5, 10, 20]:
        k_df[str(i)] = average_price_ext(k_df['Close'], i)

    f_path = 'C://Users//David//anaconda3//envs//py36_32//lib//site-packages//zz.py'

    with open(f_path, 'w', encoding='utf8') as f:
        t_text = 'def data_call(t_start,t_end):\n'
        f.write(t_text)
        t_text = '\timport numpy as np\n'
        f.write(t_text)
        t_text = '\timport pandas as pd\n'
        f.write(t_text)
        t_text = '\tdata_dic = {\n'
        f.write(t_text)
        data_write(f, k_df.loc[0])
        for i in range(1, len(k_df)):
            f.write(',\n')
            data_write(f, k_df.loc[i])
        f.write('}\n')


        r_text = [ '\tt_pass = False\n',    '\tf_list = []\n',    '\tfor obj in data_dic:\n',    '\t\tif t_start == obj:\n',
                    '\t\t\tt_pass = True\n',    '\t\tif t_pass:\n',    '\t\t\tf_list.append([obj] + list(data_dic[obj]))\n',
                    '\t\tif t_end == obj:\n',    '\t\t\tf_list.append([obj] + list(data_dic[obj]))\n',    '\t\t\tt_pass = False\n',
                    '\tr_data = pd.DataFrame(columns = [\'Index\', \'High\', \'Low\', \'Open\', \'Close\', \'Volume\', \'adjclose\', \'real\', \'5\', \'10\', \'20\'])\n',
                   '\tfor i in range(len(f_list)):\n',
                    '\t\tr_data.loc[i] = f_list[i]\n', '\treturn r_data\n']
        for obj in r_text:
            f.write(obj)